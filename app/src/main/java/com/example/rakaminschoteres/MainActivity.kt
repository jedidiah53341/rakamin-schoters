package com.example.rakaminschoteres

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private val list = ArrayList<Article>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rvPengguna:RecyclerView = findViewById(R.id.rvPengguna)
        val textResponseCode: TextView = findViewById(R.id.textResponseCode)
        var textResponseCode2: TextView = findViewById(R.id.textResponseCode2)

        rvPengguna.setHasFixedSize(true)
        rvPengguna.layoutManager = LinearLayoutManager(this)

        RetrofitClient.instance.getTopHeadlines("us","bc3f0f049732468fb6cadff792838122").enqueue(object:
            Callback<TopHeadlinesResponse>{
            override fun onResponse(
                call: Call<TopHeadlinesResponse>,
                response: Response<TopHeadlinesResponse>
            ) {
                val responseCode:String = response.code().toString()
                textResponseCode.text = "Kode respon : " + responseCode
                textResponseCode2.text = "Total artikel : " + response.body()?.totalResults.toString()

                val numberOfArticles = response.body()?.totalResults

                response.body()?.articles?.let { list.addAll(it) }
                var adapter = TopHeadlinesAdapter(list)
                rvPengguna.adapter = adapter

            }

            override fun onFailure(call: Call<TopHeadlinesResponse>, t: Throwable) {
                textResponseCode.text = "failure"


            }
        })


    }
}