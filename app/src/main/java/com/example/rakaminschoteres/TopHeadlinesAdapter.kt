package com.example.rakaminschoteres

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class TopHeadlinesAdapter(private val list : ArrayList<Article>):RecyclerView.Adapter<TopHeadlinesAdapter.HeadlinesViewHolder>() {
    inner class HeadlinesViewHolder (itemView: View):RecyclerView.ViewHolder(itemView) {
        val textData:TextView
        val author:TextView
        val title:TextView
        val description:TextView
        val url:TextView
        init {
            textData = itemView.findViewById(R.id.source)
            author = itemView.findViewById(R.id.author)
            title = itemView.findViewById(R.id.titlee)
            description = itemView.findViewById(R.id.description)
            url = itemView.findViewById(R.id.url)
        }

        fun bind(Article : Article) {
            var data:String = "source : ${Article.source}\n"
                    textData.text = data
            data = "${Article.author}\n"
            author.text = data
            data = "${Article.title}\n"
            title.text = data
            data = "${Article.description}\n"
            description.text = data
            data = "link : ${Article.url}\n"
            url.text = data
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeadlinesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pengguna, parent, false)
        return HeadlinesViewHolder(view)
    }

    override fun onBindViewHolder(holder: HeadlinesViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

}